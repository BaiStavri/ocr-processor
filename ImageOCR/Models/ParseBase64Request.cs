﻿namespace ImageOCR.Models
{
    public class ParseBase64Request
    {
        public string Base64 { get; set; }
    }
}