﻿namespace ImageOCR.Models
{
    public class ParseResponse
    {
        public string Text { get; set; }
    }
}