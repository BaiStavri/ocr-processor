﻿using ImageOCR.Models;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Web.Http;
using Tesseract;

namespace ImageOCR.Controllers
{
    public class ImagesController : ApiController
    {
        [ActionName("url")]
        [HttpPost]
        public ParseResponse ParseUrl([FromBody] ParseRequest request)
        {
            WebClient client = new WebClient();
            Stream stream = client.OpenRead(request.Url);
            Bitmap bitmap = new Bitmap(stream);

            return new ParseResponse()
            {
                Text = GetText(bitmap)
            };
        }

        [ActionName("base64")]
        [HttpPost]
        public ParseResponse ParseBase64([FromBody] ParseBase64Request request)
        {
            Byte[] bitmapData = Convert.FromBase64String(FixBase64ForImage(request.Base64));
            MemoryStream streamBitmap = new MemoryStream(bitmapData);
            Bitmap bitmap = new Bitmap((Bitmap)Image.FromStream(streamBitmap));

            return new ParseResponse()
            {
                Text = GetText(bitmap)
            };
        }

        private string GetText(Bitmap bitmap)
        {
            string text;
            try
            {
                using (var engine = new TesseractEngine(
                    System.Web.Hosting.HostingEnvironment.MapPath(@"~\App_Data\tessdata"), "eng", EngineMode.Default))
                {
                    using (var page = engine.Process(bitmap))
                    {
                        text = page.GetText();
                    }
                }
            }
            catch (Exception e)
            {
                text = e.Message;
            }

            return text;
        }

        private string FixBase64ForImage(string Image)
        {
            System.Text.StringBuilder sbText = new System.Text.StringBuilder(Image, Image.Length);
            sbText.Replace("\r\n", String.Empty); sbText.Replace(" ", String.Empty);
            return sbText.ToString();
        }
    }
}
